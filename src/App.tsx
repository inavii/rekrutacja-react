import {useCallback, useMemo, useState} from 'react'
import Table from '@components/Table/Table'
import Tabs from '@components/Tabs/Tabs'
import Accordion from '@components/Accordion/Accordion'
import {explodeObject} from "@helper/explodeObject";
import './App.css'
import {useFetch} from "./hooks/useFetch";

interface Author {
    name?: string,
    active?: boolean,
}

interface DataTable {
    id: number,
    app: string,
    author: Author | null | undefined
    type: string,
    version: string,
}

function App() {
    const [activeFilter, setActiveFilter] = useState<string>('');
    const {errors, apiData} = useFetch<DataTable>("./dataset.json");

    const columns = useMemo(() => (
        [
            {
                header: "Application Name",
                tableIndex: "app",
            },
            {
                header: "Author",
                tableIndex: "author.name",
            },
            {
                header: "Version",
                tableIndex: "version",
            },
        ]
    ), [])

    const options = useMemo(() => explodeObject(apiData, (item: any) => item.type), [apiData])
    const filter = useCallback((condition) => apiData?.filter(item => item.type?.includes(activeFilter) && condition(item.version)), [activeFilter])

    return (
        <div className="App">
            {errors ? (<h1>{errors}</h1>) : (
                <>
                    <Tabs data={options} filter={(filter: string) => setActiveFilter(filter)}/>

                    <Accordion title="Mature Applications" open={true}>
                        <Table data={filter((version: string) => version >= '1.0.0')}
                               columns={columns}/>
                    </Accordion>

                    <Accordion title="Beta Applications">
                        <Table data={filter((version: string) => version < '1.0.0')}
                               columns={columns}/>
                    </Accordion>
                </>
            )}
        </div>
    )
}

export default App