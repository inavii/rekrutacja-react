import {useEffect, useState} from "react";

export const useFetch = <T extends any>(url: string): { isLoading: boolean; apiData: any[]; errors: any } => {
    const [isLoading, setIsLoading] = useState<boolean>(false);
    const [apiData, setApiData] = useState<T[]>([]);
    const [errors, setErrors] = useState<any>(false);

    useEffect(() => {
        setIsLoading(true);
        const fetchData = async () => {
            try {
                const response = await fetch(url);
                const data = await response?.json();

                setApiData(data);
                setIsLoading(false);
            } catch (error) {
                setErrors(error);
                setIsLoading(false);
            }
        };
        fetchData();
    }, [url]);

    return {isLoading, apiData, errors};
};
