import {fireEvent, render, screen} from "@testing-library/react";
import Accordion from "@components/Accordion/Accordion";

describe('Test Accordion component', () => {
    const componentChildren = <h1>Lorem ipsum</h1>

    it('should render title Accordion', () => {
        render(<Accordion title='Accordion'>{componentChildren}</Accordion>)

        expect(screen.getByText('Accordion')).toBeInTheDocument()
    });

    it('should render component children', () => {
        render(<Accordion title='Accordion'>{componentChildren}</Accordion>)

        expect(screen.getByText('Lorem ipsum')).toBeInTheDocument()
    });

    it('should be have class collapsed', () => {
        render(<Accordion title='Accordion'>{componentChildren}</Accordion>)

        expect(screen.getByTestId('accordionItem')).toHaveClass('collapsed')
    });

    it('should be not have class collapsed when props open = true', () => {
        render(<Accordion title='Accordion' open={true}>{componentChildren}</Accordion>)

        expect(screen.getByTestId('accordionItem')).not.toHaveClass('collapsed')
    });

    it('when accordion is open after click should be have class collapsed', () => {
        render(<Accordion title='Accordion' open={true}>{componentChildren}</Accordion>)

        fireEvent.click(screen.getByText('Accordion'))

        expect(screen.getByTestId('accordionItem')).toHaveClass('collapsed')
    });
})