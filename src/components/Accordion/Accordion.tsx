import React, {FC, useState} from "react";
import {ContainerAccordion} from "./styles";
import {ReactComponent as ArrowSvg} from "@assets/arrow.svg";

interface AccordionPropsInterface {
    title: string,
    open?: boolean
    children: React.ReactElement
}

const Accordion: FC<AccordionPropsInterface> = ({title, open = false, children}) => {
    const [isOpen, setOpen] = useState<boolean>(open)

    return (
        <ContainerAccordion>
            <div className='accordion-title' onClick={() => setOpen(!isOpen)}>
                {title}
                <ArrowSvg className={isOpen ? "icon" : "icon rotate"} viewBox="0 0 266 438" width={10}/>
            </div>

            <div data-testid='accordionItem' className={`accordion-item ${!isOpen ? 'collapsed' : ''}`}>
                <div className="accordion-content">{children}</div>
            </div>
        </ContainerAccordion>
    )
}

export default Accordion