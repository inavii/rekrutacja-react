import styled from 'styled-components'

export const ContainerTable = styled.table`
  table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    padding: 10px 30px;
  }

  tr {
    cursor: pointer;
    transition: 0.3s;

    &:hover {
      background: green;
      color: #fff;
    }
  }
`