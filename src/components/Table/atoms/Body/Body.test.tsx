import {render, screen} from "@testing-library/react";
import Body from "@components/Table/atoms/Body/Body";

describe('Test Body component', () => {
    const columns = [
        {
            header: "Application Name",
            tableIndex: "app",
        },
        {
            header: "Author",
            tableIndex: "author.name",
        },
        {
            header: "Version",
            tableIndex: "version",
        },
    ]

    const data = [
        {
            id: 1,
            app: 'Voyatouch',
            author: {
                name: 'Sile Gussie',
                active: true,
            },
            type: 'IT',
            version: '0.1.7',
        },
    ]

    beforeEach(() => {
        const table = document.createElement('table');

        render(<Body columns={columns} data={data}/>, {
            container: document.body.appendChild(table)
        })
    })

    it('should render app value => Voyatouch', () => {
        expect(screen.getByText('Voyatouch')).toBeInTheDocument()
    });

    it('should render author name => Sile Gussie', () => {
        expect(screen.getByText('Sile Gussie')).toBeInTheDocument()
    });

    it('should render version value => 0.1.7', () => {
        expect(screen.getByText('0.1.7')).toBeInTheDocument()
    });
})