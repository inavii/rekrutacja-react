import React, {FC, ReactNode} from "react";
import {TablePropsInterface} from "../../Table";
import {deepSearchObject} from "@helper/deepSearchObject";

const Body: FC<TablePropsInterface> = ({columns, data}) => {

    const rowHandler = (event: React.MouseEvent<HTMLElement>) => {
        window.location.href = `#${event.currentTarget?.children[0]?.textContent}`
    }

    const renderRow = (row: object): string[] => {
        const results: [] = [];
        columns.forEach(item => deepSearchObject(row, item.tableIndex, results))
        return results;
    }

    const renderContent = (): ReactNode => {
        return data?.map((row: object, index: number) => {
            return (
                <tr key={index} onClick={rowHandler}>
                    {renderRow((row))?.map((item, index) => <td key={index}>{item}</td>)}
                </tr>
            );
        });
    }

    return <tbody>{renderContent()}</tbody>
}

export default Body
