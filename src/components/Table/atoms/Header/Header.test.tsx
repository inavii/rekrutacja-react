import {render, screen} from "@testing-library/react";
import Header from "@components/Table/atoms/Header/Header";

describe('Test Header component', () => {
    const columns = [
        {
            header: "Application Name",
            tableIndex: "app",
        },
        {
            header: "Author",
            tableIndex: "author.name",
        },
        {
            header: "Version",
            tableIndex: "version",
        },
    ]

    it('should render three th elements', () => {
        const table = document.createElement('table');

        render(<Header columns={columns}/>, {
            container: document.body.appendChild(table)
        })

        expect(screen.getAllByTestId('header')).toHaveLength(3)
    });

})