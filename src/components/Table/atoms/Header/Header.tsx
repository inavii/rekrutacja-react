import {FC} from "react";
import {TableColumns} from "../../Table";

interface HeaderPropsInterface {
    columns: TableColumns[]
}

const Header: FC<HeaderPropsInterface> = ({columns}) => {
    return (
        <thead>
        <tr>
            {columns?.map(column => <th key={column.header} data-testid='header'>{column.header}</th>)}
        </tr>
        </thead>
    )
}

export default Header