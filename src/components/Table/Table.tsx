import {FC, useMemo} from "react";
import Header from "./atoms/Header/Header";
import Body from "./atoms/Body/Body";
import {ContainerTable} from "@components/Table/styles";

export interface TableColumns {
    header: string,
    tableIndex: string,
}

export interface TablePropsInterface {
    data: any
    columns: TableColumns[]
}

const Table: FC<TablePropsInterface> = ({columns, data}) => {

    const render = useMemo(() => (
        <>
            <Header columns={columns}/>
            <Body columns={columns} data={data}/>
        </>
    ), [columns, data])

    return <ContainerTable>{render}</ContainerTable>
}

export default Table