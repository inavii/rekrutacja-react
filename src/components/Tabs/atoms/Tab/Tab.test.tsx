import {fireEvent, render, screen} from "@testing-library/react";
import Tab from "@components/Tabs/atoms/Tab/Tab";

describe('Test Tab component', () => {
    const mockFunction = jest.fn();

    it('should render tab name IT', () => {
        render(<Tab index={1} tabName='IT' activeTabIndex={0} callBackFunction={mockFunction}/>)

        expect(screen.getByText('IT')).toBeInTheDocument()
    });

    it('tab should be have class active', () => {
        render(<Tab index={2} tabName='IT' activeTabIndex={2} callBackFunction={mockFunction}/>)

        expect(screen.getByText('IT')).toHaveClass('active')
    });

    it('tab should be attribute data-index = 1', () => {
        render(<Tab index={1} tabName='IT' activeTabIndex={1} callBackFunction={mockFunction}/>)

        expect(screen.getByText('IT').getAttribute('data-index')).toEqual("1")
    });

    it('callBack function should be called when clicked tab', () => {
        render(<Tab index={1} tabName='IT' activeTabIndex={1} callBackFunction={mockFunction}/>)

        fireEvent.click( screen.getByText('IT'))

        expect(mockFunction).toHaveBeenCalled()
    });
})