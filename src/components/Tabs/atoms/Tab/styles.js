import styled from 'styled-components'

export const ContainerTab = styled.div`
  div {
    display: block;
    height: 100%;
    border: 1px solid #000;
    padding: 10px 20px;
    transition: 0.3s;
    cursor: pointer;
    position: relative;

    &.active {
      background: green;
      color: #fff;
    }

    &:hover {
      background: green;
      color: #fff;
    }
  }
`