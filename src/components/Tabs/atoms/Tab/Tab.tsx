import React, {FC} from "react";
import {ContainerTab} from "./styles";

interface TabPropsInterface {
    index: number
    tabName: string
    activeTabIndex: number
    callBackFunction: React.MouseEventHandler<HTMLElement>
}

const Tab: FC<TabPropsInterface> = ({index, tabName, activeTabIndex, callBackFunction}) => {

    return (
        <ContainerTab>
            <div
                data-testid='tab'
                role='tab'
                aria-selected={index === activeTabIndex}
                title={tabName}
                data-index={index}
                className={index === activeTabIndex ? 'active' : ''}
                onClick={callBackFunction}>{tabName}
            </div>
        </ContainerTab>
    )
}

export default Tab