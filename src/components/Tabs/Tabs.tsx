import React, {FC, useEffect, useState} from "react";
import Tab from "./atoms/Tab/Tab";
import {ContainerTabs} from "./styles";

interface TabsPropsInterface {
    data: string[]
    filter: Function
    defaultActive?: number
}

const Tabs: FC<TabsPropsInterface> = ({data, filter, defaultActive = 0}) => {

    const [activeTabs, setActiveTab] = useState<number>(defaultActive);

    const callBackFunction = (event: React.MouseEvent<HTMLElement>): void => {
        const target = event.target as HTMLElement;
        filter(target.title)

        setActiveTab(parseInt(target.dataset.index!!))
    }

    useEffect(() => {
        filter(data && data[0])
    }, [data])

    return (
        <ContainerTabs>
            {data?.map((item, index) =>
                <Tab key={index}
                     index={index}
                     tabName={item}
                     activeTabIndex={activeTabs}
                     callBackFunction={callBackFunction}/>)}
        </ContainerTabs>
    )
}

export default Tabs