import {fireEvent, render, screen} from "@testing-library/react";
import Tabs from "@components/Tabs/Tabs";

describe('Test integration Tabs component', () => {
    const mockFunction = jest.fn();
    const data = ['IT', 'Entertainment', 'Financial', 'Productivity']

    it('should render 4 tab', () => {
        render(<Tabs dataset={data} filter={mockFunction}/>)

        expect(screen.getAllByTestId('tab')).toHaveLength(4)
    });

    it('callBack function should be called with param "Financial" when clicked tab Financial', () => {
        render(<Tabs dataset={data} filter={mockFunction}/>)

        fireEvent.click(screen.getByText('Financial'))

        expect(mockFunction).toHaveBeenCalledWith('Financial')
    });

    it('tab "Financial" should have active class when props defaultActive = 2', () => {
        render(<Tabs dataset={data} filter={mockFunction} defaultActive={2}/>)

        expect(screen.getByText('Financial')).toHaveClass('active')
    });
})