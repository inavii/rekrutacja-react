import styled from 'styled-components'

export const ContainerTabs = styled.div`
  padding: 5px 15px;
  display: flex;
  max-width: max-content;
  margin: 50px auto;
  align-items: center;
`