import {deepSearchObject} from "../deepSearchObject";

describe('deep search Object function', () => {

    const data = {
        id: 1,
        app: 'Voyatouch',
        author: {
            name: 'Sile Gussie',
            active: true,
        },
        type: 'IT',
        version: '0.1.7',
    }

    let results: string[] = [];

    beforeEach(() => {
        results = [];
    })

    it('should return author name as array  ["Sile Gussie"]', () => {

        deepSearchObject(data, 'author.name', results)

        expect(results).toEqual(['Sile Gussie'])
    });

    it('should return app value as array  ["Voyatouch"]', () => {

        deepSearchObject(data, 'app', results)

        expect(results).toEqual(['Voyatouch'])
    });

    it('should return empty string as value  [""]', () => {

        const data = {
            id: 1,
            app: 'Voyatouch',
            author: {},
            type: 'IT',
            version: '0.1.7',
        }

        deepSearchObject(data, 'author.name', results)

        expect(results).toEqual([''])
    });

    it('should return value: app , author name and version', () => {

        const columns = ['app', 'author.name', 'version']

        columns.forEach(column => deepSearchObject(data, column, results))

        expect(results).toEqual(['Voyatouch', 'Sile Gussie', '0.1.7'])
    });
})