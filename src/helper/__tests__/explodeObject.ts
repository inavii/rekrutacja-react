import {explodeObject} from "../explodeObject";

describe('explode Object function', () => {
    const data = [
        {
            id: 1,
            app: 'Voyatouch',
            author: {
                name: 'Sile Gussie',
                active: true,
            },
            type: 'IT',
            version: '0.1.7',
        },
        {
            id: 2,
            app: 'Stringtough',
            author: {
                name: 'Helena Ducarne',
                active: true,
            },
            type: 'IT',
            version: '0.6.3',
        },
        {
            id: 3,
            app: 'Greenlam',
            author: {},
            type: 'Financial',
            version: '0.6.3',
        },
        {
            id: 4,
            app: 'Gembucket',
            author: {
                name: 'Merilyn Bedome',
                active: true,
            },
            type: null,
            version: '0.6.4',
        },
    ]


    it('should return array [IT , Financial]', () => {
        const results = explodeObject(data, (item: any) => item.type);

        expect(results).toEqual(["IT", 'Financial'])
    });
})