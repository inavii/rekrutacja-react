export const explodeObject = (data: any, condition: (item: object) => boolean): string[] => {
    return Array.from(new Set(data.map((item: object) => condition(item)).filter((element: any) => element)));
}