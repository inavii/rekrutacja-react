export const deepSearchObject = (obj: any, field: string, results: string[]): void => {
    const fieldMap: string[] = field.split('.');

    if (!obj[fieldMap[0]]) {
        results.push('');
        return
    }

    if (fieldMap.length === 1) {
        results.push(obj[fieldMap[0]]);
        return;
    }

    deepSearchObject(obj[fieldMap[0]], field.substr(field.indexOf('.') + 1), results);
}